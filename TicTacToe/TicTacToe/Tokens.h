#pragma once

enum class TokenSet
{
	Space = ' ',
	Naught = 'O',
	Cross = 'X'
};