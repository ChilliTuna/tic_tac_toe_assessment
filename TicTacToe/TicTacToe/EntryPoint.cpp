#include "GameBoard.h"

int main()
{
	//Game loop - never exits
	while (true)
	{
		std::cout << "Input horizontal position then vertical position, separated by a space" << std::endl;
		//Creates a new game board
		GameBoard board;
		//Prints the board
		board.PrintBoard();
		//Secondary game loop - exits at the end of each game
		while (true)
		{
			short turnResults;
			//Returns 0 if the game hasn't concluded
			//Returns 1 if the player wants to replay
			//Returns 2 if the player wants to quit
			//Lets the crosses user play the game and returns the above
			turnResults = board.PlayTurn(TokenSet::Cross);
			if (turnResults == 1)
			{
				//Exits secondary game loop
				break;
			}
			else if (turnResults == 2)
			{
				//Exits program
				return 0;
			}
			
			//Same as above, but for the naughts player
			turnResults = board.PlayTurn(TokenSet::Naught);
			if (turnResults == 1)
			{
				break;
			}
			else if (turnResults == 2)
			{
				return 0;
			}
		}
	}
}