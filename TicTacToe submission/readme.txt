To compile:
1)Open solution in Visual Studio.
2)Go to the "Build" tab at the top of VS.
3)Go to "Configuration manager"
4)Under "Active solution configuration" choose "Release" and close the configuration manager
5)Go back to the "Build" tab
6)Press "Build Solution"

To execute:
1)After compiling, locate the "TicTacToe" folder
4)Open "Release"
5)Run "TicTacToe.exe"

How to play:
The objective of the game is to place 3 of your own "tokens" next to each other
vertically, horizontally or diagonally.
There are two players. Player X (who uses the X token) and Player O (who uses the O token).
Player X starts places a token, then player O does the same. The players take turns in placing
a token on the board. Neither player can place a token where one already exists.
If the board is filled and neither player has won, the game is considered a draw.
Below is an example of an X player victory:

  X | O | X 
 ---+---+---
    | X | O 
 ---+---+---
  O |   | X 

*Note how X has 3 tokens lined up diagonally

Tokens are placed by typing the position the players wishes to place their token.
The player types the horizontal position then the vertical position, separated
by a space, then presses "Enter" on the keyboard.
Below is an example board, detailing all the positions:

 1 1|2 1|3 1
 ---+---+---
 1 2|2 2|3 2
 ---+---+---
 1 3|3 2|3 3

Once the game has been finished and when prompted, enter y to restart the game or enter n to exit.