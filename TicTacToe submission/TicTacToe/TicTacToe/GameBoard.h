#pragma once

#include "Tokens.h"
#include "DataValidation.h"

class GameBoard
{
public:
	static const int boardWidth = 3;
	static const int boardHeight = 3;
	int boardValues[boardWidth][boardHeight];

	GameBoard();
	void ResetBoard();
	void PrintBoard();
	void PlaceOnBoard(TokenSet token);
	short CheckForWin(TokenSet token);
	short PlayTurn(TokenSet token);
};
