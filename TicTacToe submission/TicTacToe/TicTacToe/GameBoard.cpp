#include "GameBoard.h"

//There were many cases of std:: being used, so the std namespace was included instead
using namespace std;

///<summary>Sets the board as blank, upon creation
///</summary>
GameBoard::GameBoard()
{
	ResetBoard();
}

///<summary>Sets the board to blank (not NULL)
///</summary>
void GameBoard::ResetBoard()
{
	//Iterates through every position in the boardValues array and sets it to a space character
	for (int i = 0; i < boardWidth; i++)
	{
		for (int j = 0; j < boardHeight; j++)
		{
			boardValues[i][j] = (char)TokenSet::Space;
		}
	}
}

///<summary>Prints the board including all current tokens
///</summary>
void GameBoard::PrintBoard()
{
	//Creates blank line
	cout << endl;
	//Iterates once for every 3 iterations of j
	for (int i = 0; i < boardHeight; i++)
	{
		for (int j = 0; j < boardWidth; j++)
		{
			//Prints a space
			cout << " ";
			//Prints the character associated with this location in the array
			cout << (char)boardValues[j][i];
			cout << " ";
			//Prints bars between each column
			if (j < boardWidth - 1)
			{
				cout << "|";
			}
		}
		cout << endl;
		//Creates lines between each row
		if (i < boardHeight - 1)
		{
			cout << "---";
			for (int i = 1; i < boardWidth; i++)
			{
				cout << "+---";
			}
			cout << endl;
		}
	}
	cout << endl;
}


///<summary>Takes console input and places the chosen token at the input location
///</summary>
void GameBoard::PlaceOnBoard(TokenSet token)
{
	//Sets the min val (which number to start counting from)
	int boardMin = 1;
	while (true)
	{
		//Creates variables for the horizontal and vertical coords
		string firstVal, secondVal;
		//Takes user input and inserts it into the variables created above
		cin >> firstVal >> secondVal;
		//Checks whether the user's first input is valid (is numerical and within chosen range)
		if (!ValidateInputMinToMax(firstVal, boardMin, boardWidth - (boardMin- 1)))
		{
			//If the input is invalid, make the user input again
			cout << "Please choose a number from " << boardMin << " to " << boardWidth - (boardMin - 1) << endl;
			continue;
		}
		//Checks whether the user's second input is valid
		if (!ValidateInputMinToMax(secondVal, boardMin, boardHeight - (boardMin - 1)))
		{
			//If the input is invalid, make the user input again
			cout << "Please choose a number from" << boardMin << " to " << boardHeight - (boardMin - 1) << endl;
			continue;
		}
		//Checks if the chosen position is blank (a space char)
		if (boardValues[stoi(firstVal) - boardMin][stoi(secondVal) - boardMin] == (char)TokenSet::Space)
		{
			//Sets the chosen position to contain the token
			boardValues[stoi(firstVal) - boardMin][stoi(secondVal) - boardMin] = (char)token;
			//Ends the function
			return;
		}
		//If the chosen position isn't blank, let the user know and ask for input again
		else
		{
			cout << "There is already a token there. Choose another position." << endl;
		}
	}
}

///<summary>Checks the chosen token against all win conditions to see if the chosen player has won
///<para>Also checks if the game is a draw</para>
///<return>Returns 0 if game is not over. Returns 1 if chosen token has won. Returns 2 if game is a draw.</return>
///</summary>
short GameBoard::CheckForWin(TokenSet token)
{
	for (int i = 0; i < boardHeight; i++)
	{
		//Counts number of chosen token per row
		int validValsRow = 0;
		//Counts number of chosen token per column
		int validValsCol = 0;
		for (int j = 0; j < boardWidth; j++)
		{
			//Checks if value at position is token. If so, add it to num of tokens in row
			if (boardValues[i][j] == (char)token)
			{
				validValsRow++;
				//Checks if the number of tokens in row is 3
				if (validValsRow == 3)
				{
					return 1;
				}
			}
			//Checks if value at position is token. If so, add it to num of tokens in column
			if (boardValues[j][i] == (char)token)
			{
				validValsCol++;
				//Checks if the number of tokens in column is 3
				if (validValsCol == 3)
				{
					return 1;
				}
			}
		}
	}
	//k = offset of each row for diagonal lines (allows upscaling of board)
	for (int k = boardWidth - 3; k >= 0; k--)
	{
		//Counts number of chosen token per left-right diagonal
		int validDiagLR = 0;
		//Counts number of chosen token per right-left diagonal
		int validDiagRL = 0;
		for (int i = 0; i < boardHeight; i++)
		{
			//Checks if value at position is token. If so, add it to num of tokens in l-r diagonal
			if (boardValues[i + k][i] == (char)token)
			{
				validDiagLR++;
				if (validDiagLR == 3)
				{
					return 1;
				}
			}
			//Checks if value at position is token. If so, add it to num of tokens in r-l diagonal
			if (boardValues[i][(boardWidth - 1) - i - k] == (char)token)
			{
				validDiagRL++;
				if (validDiagRL == 3)
				{
					return 1;
				}
			}
		}
	}
	//k = offset of each row for diagonal lines (allows upscaling of board)
	for (int k = boardHeight - 3; k >= 0; k--)
	{
		//Counts number of chosen token per left-right diagonal
		int validDiagLR = 0;
		//Counts number of chosen token per right-left diagonal
		int validDiagRL = 0;
		for (int i = 0; i < boardHeight; i++)
		{
			//Checks if value at position is token. If so, add it to num of tokens in l-r diagonal
			if (boardValues[i][i + k] == (char)token)
			{
				validDiagLR++;
				if (validDiagLR == 3)
				{
					return 1;
				}
			}
			//Checks if value at position is token. If so, add it to num of tokens in r-l diagonal
			if (boardValues[boardHeight - (boardHeight - 1 - i - k)][boardHeight - 1 - i] == (char)token)
			{
				validDiagRL++;
				if (validDiagRL == 3)
				{
					return 1;
				}
			}
		}
	}
	//Number of slots that are full
	int fullSlots = 0;
	//Iterates through all positions in array. If all slots are full, return 2
	for (int i = 0; i < boardHeight; i++)
	{
		for (int j = 0; j < boardWidth; j++)
		{
			if (boardValues[i][j] != (char)TokenSet::Space)
			{
				fullSlots++;
				//Checks if number of full slots == number of slots
				if (fullSlots == boardHeight * boardWidth)
				{
					//Represents a draw
					return 2;
				}
			}
		}
	}
	//Return 0 if game hasn't been won and is not a draw (game is still ongoing)
	return 0;
}

///<summary>Handles the actions done during each turn and asks the player if they want to replay (once the game concludes)
///<param name='TokenSet token'></param>
///<para></para>
///<return>Returns 0 if game is not over. Returns 1 if player wants to play again. Returns 2 if player wants to quit.</return>
///</summary>
short GameBoard::PlayTurn(TokenSet token)
{
	//Prints who's turn it is
	cout << "It's " << (char)token << "'s turn." << endl;
	//Places chosen token on the board
	PlaceOnBoard(token);
	//Print the board
	PrintBoard();
	//Checks if the game has been won and assigns the return value to a variable
	short winValue = CheckForWin(token);
	//Switch statement that does different things depending on the results of CheckForWin()
	switch (winValue)
	{
	//Game is not resolved: do nothing
	case 0:
		return 0;
	
	//Lets user know who won
	case 1:
		cout << (char)token << " has won! \nPlay again? (y/n)" << endl;
		break;

	//Lets user know the game is a draw
	case 2:
		cout << "Nobody won. It was a tie! \nPlay again? (y/n)" << endl;
		break;
	}
	//Checks whether the user wishes to continue
	if (CheckForContinue())
	{
		return 1;
	}
	else
	{
		return 2;
	}
}