#include "DataValidation.h"

///<summary>Checks that the input is only a number and that it fits within the min amd max parameters
///<para></para>
///<return>Returns true if input is valid. Returns false if not.</return>
///</summary>
bool ValidateInputMinToMax(std::string input, int min, int max)
{
	//Checks that each character of the input string is a number. If not, return false
	for (char character : input)
	{
		if (character < 48 || 57 < character)
		{
			return false;
		}
	}
	//Checks that the input is within the range of the params. If not, return false
	if (std::stoi(input) < min || max < std::stoi(input))
	{
		return false;
	}
	return true;
}

///<summary>Checks whether player wants to restart
///<para></para>
///<return>Returns true if player wants to continue. Returns false if they want to quit</return>
///</summary>
bool CheckForContinue()
{
	while (true)
	{
		std::string restartString;
		//Takes user input
		std::cin >> restartString;
		//If the length of the input isn't 1, ask them to reenter
		if (restartString.length() == 1)
		{
			//Converts the input to lowercase
			char answer = tolower(restartString[0]);
			//If the input is y, return true, if its n, return false, if its neither, ask user to reenter
			if (answer == 'y')
			{
				return true;
			}
			else if (answer == 'n')
			{
				return false;
			}
		}
		std::cout << "Please enter either \"y\" or \"n\"" << std::endl;
	}
}